import spotipy
from spotipy.oauth2 import SpotifyClientCredentials

# PreReqs:
# pip install spotipy
# export SPOTIPY_CLIENT_ID='?'
# export SPOTIPY_CLIENT_SECRET='?'
# export SPOTIPY_REDIRECT_URI='http://localhost'

keys = {
    0: 'C',
    1: 'C#/Db',
    2: 'D',
    3: 'D#/Eb',
    4: 'E',
    5: 'F',
    6: 'F#/Gb',
    7: 'G',
    8: 'G#/Ab',
    9: 'A',
    10: 'A#',
    11: 'B'
}


def get_keys_of_tracks(tracks, sp, key_dict):
    uri_to_name = {}
    chunks = []
    cur_chunk = []
    # Get all tracks
    for i, item in enumerate(tracks['items']):
        cur_chunk.append(item['track']['uri'])
        uri_to_name[item['track']['uri']] = item['track']['name']
        if i != 0 and i % 40 == 0:
            chunks.append(cur_chunk)
            cur_chunk = []
    chunks.append(cur_chunk)
    cur_chunk = []

    # Get all audio features
    for chunk in chunks:
        try:
            audio_arr = sp.audio_features(chunk)
        except Exception as e:
            print("error with chunk:"+str(chunk))
            continue

        # Add to dict the key and, as a subdict, the mode
        for audio in audio_arr:
            key_inner = key_dict.get(audio['key'], {})
            mode_dict = key_inner.get(audio['mode'], [])
            name = uri_to_name.get(audio['uri']);
            mode_dict.append(name)
            key_inner[audio['mode']] = mode_dict
            key_dict[audio['key']] = key_inner

    return key_dict

username='maxedmalice'

def key_analyzer(username):
    client_credentials_manager = SpotifyClientCredentials()
    sp = spotipy.Spotify(client_credentials_manager=client_credentials_manager)
    sp.trace=False
    playlists = sp.user_playlists(username)
    key_dict = {}
    for playlist in playlists['items']:
        if playlist['owner']['id'] == username:
            print(playlist['name'])
            print('total tracks: '+str(playlist['tracks']['total']))
            results = sp.user_playlist(username, playlist['id'], fields="tracks,next")
            tracks = results['tracks']
            if playlist['tracks']['total'] == 0:
                continue
            key_dict = get_keys_of_tracks(tracks, sp, key_dict)
            while tracks['next']:
                tracks = sp.next(tracks)
                key_dict = get_keys_of_tracks(tracks, sp, key_dict)


    # (Print all songs in key of Cmaj)
    # for track in key_dict.get(0).get(1):
    #     print(track)

    count = 0;
    for key in key_dict.keys():
        major = len(key_dict[key][1])
        minor = len(key_dict[key][0])
        print("There are "+str(major)+" songs in the major key of "+keys[key]+" and "+str(minor)+" songs in the minor")
        count+=major+minor

    print("Total songs analyzed: "+str(count))

key_analyzer(username)
