# whatTheKey

## Simple python script to check the key of a given users library by examining all playlist

# PreReqs:
* pip install spotipy
* export SPOTIPY_CLIENT_ID='?'
* export SPOTIPY_CLIENT_SECRET='?'
* export SPOTIPY_REDIRECT_URI='http://localhost'

Sample final output:
```
There are 322 songs in the major key of C and 44 songs in the minor
There are 91 songs in the major key of C#/Db and 64 songs in the minor
There are 264 songs in the major key of D and 42 songs in the minor
There are 23 songs in the major key of D#/Eb and 22 songs in the minor
There are 116 songs in the major key of E and 85 songs in the minor
There are 119 songs in the major key of F and 60 songs in the minor
There are 71 songs in the major key of F#/Gb and 62 songs in the minor
There are 214 songs in the major key of G and 36 songs in the minor
There are 76 songs in the major key of G#/Ab and 39 songs in the minor
There are 185 songs in the major key of A and 112 songs in the minor
There are 63 songs in the major key of A# and 37 songs in the minor
There are 96 songs in the major key of B and 111 songs in the minor
Total songs analyzed: 2354
```